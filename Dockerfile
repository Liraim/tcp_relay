FROM rust:1.56.1 as build

WORKDIR /build
COPY . .

RUN cargo build --release

FROM ubuntu:20.04

WORKDIR /app

COPY --from=build /build/target/release/tcp-relay ./