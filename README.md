# Network Socket tunnel

Transfers TCP or UDP packets through given connection.

# Usage

Example: 
Creates tunnel using local port 9999, so when UDP packet had come it would be transferred from one target to another

eg.: UDP packet to 127.0.0.1:9998 would be transferred to 127.0.0.1:9997 and visa versa. 
```
tcp_relay -s 127.0.0.1:9999 --target udp:127.0.0.1:9998
tcp_relay -c 127.0.0.1:9999 --target udp:127.0.0.1:9997
```