use std::net::{AddrParseError, SocketAddr};
use std::str::FromStr;

mod lib;

use clap::Parser;
use log::{error, LevelFilter};
use simplelog;
use simplelog::{ConfigBuilder, SimpleLogger};

use crate::lib::{
    new_relay_client, new_relay_server, RelayClient, RelayError, RelayServer, Target,
};

#[derive(Parser, Debug)]
struct Opts {
    #[clap(short, long)]
    target: String,
    #[clap(subcommand)]
    mode: ModeCommand,
}

#[derive(Parser, Debug)]
enum ModeCommand {
    Client(Client),
    Server(Server),
}

#[derive(Parser, Debug)]
struct Client {
    #[clap(short, long)]
    address: String,
}

#[derive(Parser, Debug)]
struct Server {
    #[clap(short, long)]
    address: String,
}

#[derive(Debug)]
enum DoError {
    TargetFormat(),
    AddressParse(AddrParseError),
    TargetProtocol(String),
    Run(RelayError),
}

impl From<AddrParseError> for DoError {
    fn from(err: AddrParseError) -> Self {
        DoError::AddressParse(err)
    }
}

impl From<RelayError> for DoError {
    fn from(err: RelayError) -> Self {
        DoError::Run(err)
    }
}

fn create_client_relay(address: String, target: String) -> Result<RelayClient, DoError> {
    let tunnel = SocketAddr::from_str(address.as_str())?;
    match target.split_once(":").ok_or(DoError::TargetFormat())? {
        ("tcp", target_address) => Ok(new_relay_client(
            tunnel,
            Target::Tcp(SocketAddr::from_str(target_address)?),
        )),
        ("udp", target_address) => Ok(new_relay_client(
            tunnel,
            Target::Udp(SocketAddr::from_str(target_address)?),
        )),
        (proto, _) => Err(DoError::TargetProtocol(format!(
            "unknown protocol: {}",
            proto
        ))),
    }
}

fn create_server_relay(address: String, target: String) -> Result<RelayServer, DoError> {
    let tunnel = SocketAddr::from_str(address.as_str())?;
    match target.split_once(":").ok_or(DoError::TargetFormat())? {
        ("tcp", target_address) => Ok(new_relay_server(
            tunnel,
            Target::Tcp(SocketAddr::from_str(target_address)?),
        )),
        ("udp", target_address) => Ok(new_relay_server(
            tunnel,
            Target::Udp(SocketAddr::from_str(target_address)?),
        )),
        (proto, _) => Err(DoError::TargetProtocol(format!(
            "unknown protocol: {}",
            proto
        ))),
    }
}

async fn start_relay(opts: Opts) -> Result<(), DoError> {
    match opts.mode {
        ModeCommand::Client(client_opts) => {
            let mut relay = create_client_relay(client_opts.address, opts.target)?;
            relay.start().await?;
        }
        ModeCommand::Server(server_opts) => {
            let mut relay = create_server_relay(server_opts.address, opts.target)?;
            relay.start().await?;
        }
    };
    Ok(())
}

#[tokio::main]
async fn main() {
    let config = ConfigBuilder::new().set_time_format_str("%+").build();
    let _ = SimpleLogger::init(LevelFilter::Info, config);
    let opts: Opts = Opts::parse();

    log::info!("Configuration: {:?}", opts);

    let results = start_relay(opts).await;

    match results {
        Ok(_) => {}
        Err(err) => error!("Failed: {:?}", err),
    }
}
