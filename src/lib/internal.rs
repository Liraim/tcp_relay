use crate::lib::RelayError;
use byteorder::ByteOrder;
use std::error::Error;
use std::fmt::{Debug, Display, Formatter};
use std::net::SocketAddr;
use tokio::io::{AsyncReadExt, AsyncWriteExt};
use tokio::net::TcpStream;
use tokio::sync::mpsc;
use tokio::time::{self, Duration};

use tokio::select;

use log::info;

pub(crate) async fn internal_channel(
    chid: String,
    connection: &mut TcpStream,
    mut reader: mpsc::Receiver<(SocketAddr, Vec<u8>)>,
    writer: mpsc::Sender<(SocketAddr, Vec<u8>)>,
    target_addr: SocketAddr,
) -> Result<(), RelayError> {
    let mut write_buffer = [0u8; 16000];
    let mut target_addr = target_addr;
    loop {
        select! {
            unpack_result = unpack2(connection) => {
                match unpack_result {
                    Ok(data) => {
                        if data.len() == 0 {
                            continue;
                        }
                        info!("[{}] conn -> chan, bytes  {}", chid, data.len());
                        writer.send((target_addr, data)).await?;
                    }
                    Err(_) => {
                        info!("[{}] failed to unpack result", chid);
                        break;
                    }
                }
            }
            timeout_result = time::timeout(Duration::from_millis(1000), reader.recv()) => {
                match timeout_result {
                    Ok(message) => {
                        match message {
                            Some((addr, data)) => {
                                target_addr = addr;
                                info!("[{}] chan -> conn, data size {}", chid, data.len());
                                byteorder::BigEndian::write_u32(&mut write_buffer, data.len() as u32);

                                write_buffer[4..4+data.len()].clone_from_slice(&data);
                                connection.write_all(&write_buffer[..4+data.len()]).await?;
                            }
                            None => {
                                info!("[{}] channel closed", chid);
                                break;
                            }
                        }
                    }
                    Err(_) => {
                        info!("ping!");
                        connection.write_all(&[0u8, 0u8, 0u8, 0u8]).await?;
                        connection.flush().await?;
                    }
                }
            }
        }
    }
    Ok(())
}

#[derive(Debug)]
struct ParseError {}

impl Display for ParseError {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        self::Debug::fmt(self, f)
    }
}

impl Error for ParseError {}

async fn unpack2(source: &mut TcpStream) -> Result<Vec<u8>, RelayError> {
    let message_size = source.read_u32().await?;
    if message_size == 0 {
        info!("pong!");
        return Ok(Vec::new());
    }
    info!("expected message size: {}", message_size);
    let mut data = vec![0u8; message_size as usize];
    source.read_exact(&mut data).await?;
    info!("message: size: {}", &data.len());
    Ok(data)
}
