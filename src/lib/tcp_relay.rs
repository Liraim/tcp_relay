use crate::lib::internal::internal_channel;
use crate::lib::{Handle, RelayError};
use std::net::SocketAddr;
use std::str::FromStr;
use tokio::io::{AsyncReadExt, AsyncWriteExt};
use tokio::net::TcpStream;

use log::error;

use async_trait::async_trait;
use tokio::select;
use tokio::sync::mpsc;

use log::info;

pub(crate) struct TcpRelayHandle {
    tunnel_stream: TcpStream,
    target_address: SocketAddr, // Use address, because we should establish connect only when data came
}

impl TcpRelayHandle {
    pub(crate) fn new(tunnel: TcpStream, target: SocketAddr) -> Self {
        TcpRelayHandle {
            tunnel_stream: tunnel,
            target_address: target,
        }
    }
}

pub(crate) async fn tcp_channel(
    chid: String,
    connection: &mut TcpStream,
    mut reader: mpsc::Receiver<(SocketAddr, Vec<u8>)>,
    writer: mpsc::Sender<(SocketAddr, Vec<u8>)>,
) -> Result<(), RelayError> {
    let stub_addr: SocketAddr = SocketAddr::from_str("127.0.0.0:1").unwrap();
    let mut buffer = [0u8; 16000];
    loop {
        select! {
            read_result = connection.read(&mut buffer) => {
                let read_size = read_result?;
                info!("[{}] raw bytes: {}", chid, read_size);
                if read_size == 0 {
                    info!("[{}] conn closed", chid);
                    break;
                }
                info!("[{}] conn -> chan, bytes  {}", chid, read_size);
                writer.send((stub_addr, Vec::from(&buffer[..read_size]))).await?;
            }
            message = reader.recv() => {
                match message {
                    Some((_, data)) => {
                        info!("[{}] chan -> conn, data size {}", chid, data.len());
                        connection.write_all(&data).await?;
                        connection.flush().await?;
                    }
                    None => {
                        break;
                    }
                }
            }
        }
    }
    Ok(())
}

#[async_trait]
impl Handle for TcpRelayHandle {
    async fn serve(&mut self) -> Result<(), RelayError> {
        self.tunnel_stream.readable().await?;
        let mut out_conn = TcpStream::connect(self.target_address).await?;
        let (writer, reader) = mpsc::channel::<(SocketAddr, Vec<u8>)>(10);
        let (writer2, reader2) = mpsc::channel::<(SocketAddr, Vec<u8>)>(10);
        let stub_addr: SocketAddr = SocketAddr::from_str("127.0.0.0:1").unwrap();
        let (tun_result, tar_result) = tokio::join!(
            internal_channel(
                "tun".to_string(),
                &mut self.tunnel_stream,
                reader2,
                writer,
                stub_addr
            ),
            tcp_channel("tar".to_string(), &mut out_conn, reader, writer2),
        );
        if let Err(err) = tun_result {
            error!("tunnel stream failed {:?}", err);
        }
        if let Err(err) = tar_result {
            error!("target stream failed {:?}", err);
        }
        Ok(())
    }
}

pub(crate) struct TcpRelayServerHandle {
    tunnel_stream: TcpStream,
    target_address: SocketAddr, // Use address, because we should establish connect only when data came
}

impl TcpRelayServerHandle {
    pub(crate) fn new(tunnel: TcpStream, target: SocketAddr) -> Self {
        TcpRelayServerHandle {
            tunnel_stream: tunnel,
            target_address: target,
        }
    }
}

#[async_trait]
impl Handle for TcpRelayServerHandle {
    async fn serve(&mut self) -> Result<(), RelayError> {
        let target_listener = tokio::net::TcpListener::bind(self.target_address).await?;
        let (mut stream, _) = target_listener.accept().await?;

        stream.readable().await?;
        let (writer, reader) = mpsc::channel::<(SocketAddr, Vec<u8>)>(10);
        let (writer2, reader2) = mpsc::channel::<(SocketAddr, Vec<u8>)>(10);
        let stub_addr: SocketAddr = SocketAddr::from_str("127.0.0.0:1").unwrap();
        let (tun_result, tar_result) = tokio::join!(
            internal_channel(
                "tun".to_string(),
                &mut self.tunnel_stream,
                reader2,
                writer,
                stub_addr
            ),
            tcp_channel("tar".to_string(), &mut stream, reader, writer2),
        );
        if let Err(err) = tun_result {
            error!("tunnel stream failed {:?}", err);
        }
        if let Err(err) = tar_result {
            error!("target stream failed {:?}", err);
        }
        Ok(())
    }
}
