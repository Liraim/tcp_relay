use std::error::Error;
use std::fmt::{Debug, Display, Formatter};
use std::net::SocketAddr;

use async_trait::async_trait;
use tokio::sync::mpsc::error::SendError;

use log::info;

mod internal;
mod tcp_relay;
mod udp_relay;

pub enum Target {
    Tcp(SocketAddr),
    Udp(SocketAddr),
}

#[derive(Debug)]
pub enum RelayError {
    IO(std::io::Error),
    ChannelSend(SendError<(SocketAddr, Vec<u8>)>),
    ChannelRecv(tokio::sync::mpsc::error::RecvError),
    // Internal(&'static str),
}

impl Display for RelayError {
    fn fmt(&self, _: &mut Formatter<'_>) -> std::fmt::Result {
        todo!()
    }
}

impl Error for RelayError {}

impl From<std::io::Error> for RelayError {
    fn from(err: std::io::Error) -> Self {
        RelayError::IO(err)
    }
}

impl From<SendError<(SocketAddr, Vec<u8>)>> for RelayError {
    fn from(err: SendError<(SocketAddr, Vec<u8>)>) -> Self {
        RelayError::ChannelSend(err)
    }
}

impl From<tokio::sync::mpsc::error::RecvError> for RelayError {
    fn from(err: tokio::sync::mpsc::error::RecvError) -> Self {
        RelayError::ChannelRecv(err)
    }
}

pub struct RelayClient {
    tunnel: SocketAddr,
    target: Target,
}

impl RelayClient {
    pub(crate) async fn start(&mut self) -> Result<(), RelayError> {
        let tunnel_connect = tokio::net::TcpStream::connect(self.tunnel).await?;
        match self.target {
            Target::Tcp(address) => {
                let mut handle = tcp_relay::TcpRelayHandle::new(tunnel_connect, address);
                handle.serve().await?;
            }
            Target::Udp(address) => {
                let mut handle = udp_relay::UdpRelayHandle::new(tunnel_connect, address);
                handle.serve().await?;
            }
        }

        Ok(())
    }
}

pub struct RelayServer {
    tunnel: SocketAddr,
    target: Target,
}

impl RelayServer {
    pub(crate) async fn start(&mut self) -> Result<(), RelayError> {
        let listener = tokio::net::TcpListener::bind(self.tunnel).await?;
        loop {
            info!("Waiting new connections...");
            let (stream, _) = listener.accept().await?;
            info!("Client connected");
            match self.target {
                Target::Tcp(address) => {
                    let mut handle = tcp_relay::TcpRelayServerHandle::new(stream, address);
                    handle.serve().await?;
                }
                Target::Udp(address) => {
                    let mut handle = udp_relay::UdpRelayServerHandle::new(stream, address);
                    handle.serve().await?;
                }
            }
        }
    }
}

#[async_trait]
trait Handle {
    async fn serve(&mut self) -> Result<(), RelayError>;
}

pub fn new_relay_client(tunnel: SocketAddr, target: Target) -> RelayClient {
    return RelayClient { tunnel, target };
}

pub fn new_relay_server(tunnel: SocketAddr, target: Target) -> RelayServer {
    return RelayServer { tunnel, target };
}
