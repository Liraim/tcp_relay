use crate::lib::{Handle, RelayError};
use log::{error, info};
use std::net::SocketAddr;
use tokio::net::{TcpStream, UdpSocket};

use crate::lib::internal::internal_channel;
use async_trait::async_trait;
use tokio::select;
use tokio::sync::mpsc;

async fn channel(
    chid: String,
    connection: &mut UdpSocket,
    mut reader: mpsc::Receiver<(SocketAddr, Vec<u8>)>,
    writer: mpsc::Sender<(SocketAddr, Vec<u8>)>,
) -> Result<(), RelayError> {
    let mut buffer = [0u8; 16000];
    loop {
        select! {
            read_result = connection.recv_from(&mut buffer) => {
                if let Err(_) = read_result {
                    info!("[{}] try skip single read err from udp socket (?)", chid);
                    continue;
                }
                let (read_size, addr) = read_result?;
                if read_size == 0 {
                    info!("[{}] conn closed", chid);
                    break;
                }
                info!("[{}] conn -> chan, bytes {}, addr: {}", chid, read_size, addr);
                writer.send((addr, Vec::from(&buffer[..read_size]))).await?;
            }
            message = reader.recv() => {
                match message {
                    Some((addr, data)) => {
                        info!("[{}] chan -> conn, data size: {}", chid, data.len());
                        let send_size = connection.send_to(&data, addr).await?;
                        if send_size < data.len() {
                            info!("[{}] chunked send", chid);
                            let mut pos = send_size;
                            loop {
                                let part = &data[pos..];
                                let size = connection.send_to(&part, addr).await?;
                                if pos + size < data.len() {
                                    pos += size;
                                }
                            }
                        }
                    }
                    None => {
                        info!("[{}] chan data received. Shutdown message", chid);
                        break;
                    }
                }
            }
        }
    }
    Ok(())
}

pub(crate) struct UdpRelayServerHandle {
    tunnel_stream: TcpStream,
    target_address: SocketAddr, // Use address, because we should establish connect only when data came
}

impl UdpRelayServerHandle {
    pub(crate) fn new(tunnel: TcpStream, target: SocketAddr) -> Self {
        UdpRelayServerHandle {
            tunnel_stream: tunnel,
            target_address: target,
        }
    }
}

#[async_trait]
impl Handle for UdpRelayServerHandle {
    async fn serve(&mut self) -> Result<(), RelayError> {
        let mut out_conn = UdpSocket::bind(self.target_address).await?;
        let mut t_buf = [0u8; 10];
        let (_, client_address) = out_conn.peek_from(&mut t_buf).await?;
        let (writer, reader) = mpsc::channel::<(SocketAddr, Vec<u8>)>(10);
        let (writer2, reader2) = mpsc::channel::<(SocketAddr, Vec<u8>)>(10);
        let (tun_result, tar_result) = tokio::join!(
            internal_channel(
                "tun".to_string(),
                &mut self.tunnel_stream,
                reader2,
                writer,
                client_address
            ),
            channel("tar".to_string(), &mut out_conn, reader, writer2),
        );
        if let Err(err) = tun_result {
            error!("tunnel stream failed {:?}", err);
        }
        if let Err(err) = tar_result {
            error!("target stream failed {:?}", err);
        }
        Ok(())
    }
}

pub(crate) struct UdpRelayHandle {
    tunnel_stream: TcpStream,
    target_address: SocketAddr, // Use address, because we should establish connect only when data came
}

impl UdpRelayHandle {
    pub(crate) fn new(tunnel: TcpStream, target: SocketAddr) -> Self {
        UdpRelayHandle {
            tunnel_stream: tunnel,
            target_address: target,
        }
    }
}

#[async_trait]
impl Handle for UdpRelayHandle {
    async fn serve(&mut self) -> Result<(), RelayError> {
        self.tunnel_stream.readable().await?;
        let mut out_conn = UdpSocket::bind("0.0.0.0:0").await?;
        let (writer, reader) = mpsc::channel::<(SocketAddr, Vec<u8>)>(10);
        let (writer2, reader2) = mpsc::channel::<(SocketAddr, Vec<u8>)>(10);
        let (tun_result, tar_result) = tokio::join!(
            internal_channel(
                "tun".to_string(),
                &mut self.tunnel_stream,
                reader2,
                writer,
                self.target_address
            ),
            channel("tar".to_string(), &mut out_conn, reader, writer2),
        );
        if let Err(err) = tun_result {
            error!("tunnel stream failed {:?}", err);
        }
        if let Err(err) = tar_result {
            error!("target stream failed {:?}", err);
        }
        Ok(())
    }
}
